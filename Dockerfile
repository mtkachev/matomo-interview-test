FROM maven:3.6.0-jdk-11-slim
WORKDIR /home/app
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn package -Dmaven.test.skip=true
ENTRYPOINT exec mvn test