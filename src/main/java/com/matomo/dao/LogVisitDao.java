package com.matomo.dao;

import com.matomo.entities.LogVisit;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;


public interface LogVisitDao extends CrudRepository<LogVisit, Integer> {

    @Modifying
    @Transactional
    @Query("DELETE FROM matomo_log_visit")
    void delete();

    int countByidvisit(String idVisit);
}
