package com.matomo.dao;

import com.matomo.entities.LogLinkVisitAction;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface LogLinkVisitActionDao extends CrudRepository<LogLinkVisitAction, Integer> {

    long countByidactionName(int name);

    @Modifying
    @Transactional
    @Query("DELETE FROM matomo_log_link_visit_action")
    void delete();

    LogLinkVisitAction getLogLinkVisitActionByidactionName(int idaction);
}
