package com.matomo.dao;

import com.matomo.entities.LogAction;
import org.springframework.data.repository.CrudRepository;

public interface LogActionDao extends CrudRepository<LogAction, Integer> {

    LogAction getLogActionByName(String name);
}
