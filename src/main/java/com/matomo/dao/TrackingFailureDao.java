package com.matomo.dao;

import com.matomo.entities.TrackingFailure;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface TrackingFailureDao extends CrudRepository<TrackingFailure, Integer> {

    @Modifying
    @Transactional
    @Query("DELETE FROM matomo_tracking_failure")
    void delete();
}
