package com.matomo.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "matomo_log_link_visit_action")
public class LogLinkVisitAction {

    @Id
    private int idsite;
    @Column
    private int idvisitor;
    @Column
    private String idvisit;
    @Column
    private int idactionName;
}
