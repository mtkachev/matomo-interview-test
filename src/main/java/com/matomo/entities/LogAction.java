package com.matomo.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "matomo_log_action")
public class LogAction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idaction;
    @Column
    private String name;
    @Column
    private long hash;
    @Column
    private int type;
    @Column
    private Integer url_prefix;
}
