package com.matomo.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "matomo_log_visit")
public class LogVisit {
    @Id
    private int idvisit;
    @Column
    private long idsite;
}
