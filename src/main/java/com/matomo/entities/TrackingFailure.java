package com.matomo.entities;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity(name = "matomo_tracking_failure")
public class TrackingFailure {
    @Column
    private int idsite;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idfailure;
    @Column
    private Date date_first_occurred;
    @Column
    private String request_url;

}
