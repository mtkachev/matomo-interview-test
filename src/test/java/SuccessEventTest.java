import com.matomo.dao.LogActionDao;
import com.matomo.dao.LogLinkVisitActionDao;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static io.restassured.RestAssured.given;

public class SuccessEventTest extends BaseTest {

    @Autowired
    private LogActionDao logActionDao;
    @Autowired
    private LogLinkVisitActionDao logLinkVisitActionDao;

    @BeforeEach
    public void setup() {
        logActionDao.deleteAll();
        logLinkVisitActionDao.delete();
    }
    @Test
    public void valid() {
        Response response = given()
                .contentType(ContentType.JSON)
                .when()
                .get("/matomo.php?action_name=search&url=https://www.google.com/search &idsite=1&rand=351459&h=18&m=13&s=3 &rec=1&apiv=1")
                .then()
                .extract().response();
        Assertions.assertEquals(200, response.statusCode());


        int idaction = logActionDao.getLogActionByName("search").getIdaction();
        Assertions.assertNotEquals(0, idaction);
        Assertions.assertEquals(1, logLinkVisitActionDao.countByidactionName(idaction));


    }
}

