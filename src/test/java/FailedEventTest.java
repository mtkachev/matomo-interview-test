import com.matomo.dao.LogActionDao;
import com.matomo.dao.LogLinkVisitActionDao;
import com.matomo.dao.TrackingFailureDao;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static io.restassured.RestAssured.given;

public class FailedEventTest extends BaseTest {

    @Autowired
    private LogActionDao logActionDao;
    @Autowired
    private LogLinkVisitActionDao logLinkVisitActionDao;
    @Autowired
    private TrackingFailureDao trackingFailureDao;

    @BeforeEach
    public void setup() {
        logActionDao.deleteAll();
        logLinkVisitActionDao.delete();
        trackingFailureDao.delete();
    }
    @Test
    public void invalid() {
        Response response = given()
                .contentType(ContentType.JSON)
                .when()
                .get("/matomo.php?action_name=View settings &idsite=8876&rand=351459&h=18&m=13&s=3 &rec=1&apiv=1&cookie=1&_idvc=19&res=320×480&")
                .then()
                .extract().response();
        Assertions.assertEquals(400, response.statusCode());

        Assertions.assertEquals(0, logActionDao.count());
        Assertions.assertEquals(0, logLinkVisitActionDao.count());
        Assertions.assertEquals(1, trackingFailureDao.count());


    }
}

