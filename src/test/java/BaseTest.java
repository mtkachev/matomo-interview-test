import com.matomo.Application;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = Application.class)
public class BaseTest {

    @Value("${rest.assured.base.uri}")
    private String baseUri;
    @Value("${rest.assured.port}")
    private Integer port;

    @BeforeEach
    public void setupRestAssured(){
        RestAssured.baseURI = "http://" + baseUri;
        RestAssured.port = port;
    }
}
